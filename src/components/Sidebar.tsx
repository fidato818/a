import React from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
// import '../Sidebar.css'
import Navigations from '../components/NavAdmin'
import {
    update_user,
    remove_user,
    // onOpen,
} from '../app/store/reducers/userReducer'
// import '../screens/Admin/styles.css'
import { useSetState } from 'ahooks'

const Sidebar = (props: { changeColor: any }) => {
    const [state, setState] = useSetState({
        blogs: [],
        books: [],
        downloads: [],
        showModal: false,
    })

    const openModal = () => {
        if (state.showModal === false) {
            setState({ showModal: true })
        } else if (state.showModal === true) {
            setState({ showModal: false })
        }
    }

    return (
        <div>
            <Navigations />
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    <nav
                        className={
                            props.changeColor
                                ? ' sb-sidenav accordion sb-sidenav-light'
                                : ' sb-sidenav accordion sb-sidenav-dark'
                        }
                        id="sidenavAccordion"
                    >
                        <div className="sb-sidenav-menu">
                            <div className="nav">
                                {/* <div className="sb-sidenav-menu-heading">Core</div> */}
                                <Link className="nav-link" href="/admin">
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-tachometer-alt"></i>
                                    </div>
                                    Dashboard
                                </Link>

                                <Link className="nav-link" href="/AddBlogs">
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-chart-area"></i>
                                    </div>{' '}
                                    Blogs
                                </Link>
                                <Link className="nav-link" href="/addDownloads">
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-table"></i>
                                    </div>{' '}
                                    Downloads
                                </Link>
                                <Link className="nav-link" href="/addbooks">
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-chart-area"></i>
                                    </div>{' '}
                                    Books
                                </Link>
                                <Link className="nav-link" href="/projects">
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-table"></i>
                                    </div>{' '}
                                    Projects
                                </Link>
                                <Link className="nav-link" href="/videos">
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-table"></i>
                                    </div>{' '}
                                    Videos
                                </Link>
                                <button
                                    className={
                                        state.showModal
                                            ? 'nav-link '
                                            : 'nav-link collapsed'
                                    }
                                    // href="#"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#collapseLayouts"
                                    aria-expanded="false"
                                    aria-controls="collapseLayouts"
                                    onClick={() => openModal()}
                                >
                                    <div className="sb-nav-link-icon">
                                        <i className="fas fa-columns"></i>
                                    </div>
                                    Clients
                                    <div className="sb-sidenav-collapse-arrow">
                                        <i className="fas fa-angle-down"></i>
                                    </div>
                                </button>
                                <div
                                    className={
                                        state.showModal
                                            ? 'collapse show'
                                            : 'collapse '
                                    }
                                    id="collapseLayouts"
                                    aria-labelledby="headingOne"
                                    data-bs-parent="#sidenavAccordion"
                                >
                                    <nav className="sb-sidenav-menu-nested nav">
                                        <Link
                                            className="nav-link"
                                            href="/clients-testimonials"
                                        >
                                            Clients Testimonials
                                        </Link>
                                        <Link
                                            className="nav-link"
                                            href="/clients-data"
                                        >
                                            Clients Comments
                                        </Link>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div className="sb-sidenav-footer">
                            <div className="small">Logged in as:</div>
                            {/* {this.state.displayEmail} */}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    )
}

// export default Books
const mapStateToProps = (state: { user: any; dw: any }) => {
    return {
        user: state.user,
        changeColor: state.dw,
    }
}

const mapDispatchToProps = (dispatch: (arg0: any) => any) => {
    return {
        store_user: (user: any) => dispatch(update_user(user)),
        logout_user: () => dispatch(remove_user()),
        // onToggle: (user: any) => dispatch(onOpen(user)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
