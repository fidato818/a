'use client'
import React, { useEffect } from 'react'

import { useSetState } from 'ahooks'
import {
    ref as ref_database,
    remove,
    onValue,
    child,
    push,
    orderByChild,
    query,
    equalTo,
} from 'firebase/database'
import { Parser } from 'html-to-react'
import Image from 'next/image'
import Link from 'next/link'

import { db } from '@/config/firebaseConfig'
interface ArrayImage {
    imageUrl: string
    width: number
    height: number
    imageLink?: string
}

const CientComponent = ({ params }: { params: { id: string } }) => {
    const [state, setState] = useSetState<any | null>({ arr: [] })
   

    useEffect(() => {
        // props.callFunc()
        const getData = () => {
            // const { id } = params || ''
            // const starCountRef = ref(db, 'clientData/')
            const starCountRef = query(
                ref_database(db, 'clientData/'),
                orderByChild('status'),
                equalTo(true)
            )

            onValue(starCountRef, (snapshot) => {
                var newArr: any[] = []
                snapshot.forEach((data) => {
                    var childData = data.val()
                    newArr.push(childData)
                })

                setState({ arr: newArr.reverse(), spinnerLoader: false })
            })
        }
        getData()
    }, [params, setState])
    const { arr } = state
    return (
        <>
            <section id="clients" className="clients">
                <div className="container">
                    <div className="section-title" data-aos="fade-up">
                        <h2>
                            Our <strong>Clients</strong>
                        </h2>
                        <p>
                            Nothing can be better than getting a review from our
                            happy clients who recommend us and trust us their
                            business.
                        </p>
                    </div>

                    <div
                        className="row no-gutters clients-wrap clearfix"
                        data-aos="fade-up"
                    >
                        {arr.map((e: any, index: number) => {
                            return (
                                <div
                                    className="col-lg-3 col-md-4 col-xs-6"
                                    key={index}
                                >
                                    <Link
                                         href={`client-projects/${e.newPostKey}`}
                                        
                                     //   href={`/${e.newPostKey}`}
                                        // target="_blank"
                                    >
                                        <div
                                            className="client-logo"
                                            style={{
                                                backgroundColor: '#9b95c1',
                                            }}
                                        >
                                            <Image
                                                src={e.weblogoURL}
                                                className="img-fluid"
                                                alt=""
                                                width={e.logoWidth}
                                             
                                                height={e.logoHeight}
                                            />
                                        </div>
                                    </Link>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </section>
        </>
    )
}
export default CientComponent
