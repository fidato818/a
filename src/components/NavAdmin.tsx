'use client'
import React from 'react'
import { usePathname, useRouter } from 'next/navigation'
import { useAppSelector } from '@/app/store/hooks'
import Link from 'next/link'
import { useEffect } from 'react'
import { signOut } from 'firebase/auth'
import { auth } from '@/config/firebaseConfig'
import { remove_user, toggle_login } from '@/app/store/reducers/userReducer'
import { useAppDispatch } from '@/app/store/hooks'
import { useSetState } from 'ahooks'
interface sample_arr {
    backToTop: boolean

    arrLinks: {
        linkName: string
        linkAddress: string
    }[]
    arrDropdown: {
        linkName: string
        linkAddress: string
    }[]
    arrDropdown2: {
        linkName: string
        linkAddress: string
    }[]
}

const NavAdmin = () => {
    const dispatch = useAppDispatch()
    const router = useRouter()
    const userFromRedux = useAppSelector(
        (state) => state.userReducer.user || ''
    )

    const [state, setState] = useSetState<sample_arr>({
        backToTop: false,
        arrLinks: [
            { linkName: 'Home', linkAddress: '/' },
            { linkName: 'Dashboard', linkAddress: '/dashboard' },

            // { linkName: 'Videos', linkAddress: '/videos' },

            // { linkName: "Project Planner", linkAddress: '/contact' },
        ],
        arrDropdown2: [
            { linkName: 'Blogs', linkAddress: '/blogs-data' },
            { linkName: 'Downloads', linkAddress: '/downloads' },
            { linkName: 'Books', linkAddress: '/books' },
            { linkName: 'Projects', linkAddress: '/projects' },
            { linkName: 'Videos', linkAddress: '/videos' },
        ],
        arrDropdown: [
            {
                linkName: 'Clients Testimonials',
                linkAddress: '/clients-testimonials',
            },

            { linkName: 'Clients Data', linkAddress: '/clients-data' },
        ],
    })
    const pathname = usePathname()
    const { email }: any = userFromRedux
    // console.log('asd Dashboards', email)
    // const []
    useEffect(() => {
        let selectHeader: HTMLElement | null = document.getElementById(
            'header'
        ) as HTMLInputElement
        const headerOffset = selectHeader.offsetTop
        let nextElement = selectHeader?.nextElementSibling
        window.addEventListener('scroll', (event) => {
            if (headerOffset - window.scrollY <= 0) {
                selectHeader?.classList.add('fixed-top')
                selectHeader?.classList.add('shadow')
                nextElement?.classList.add('scrolled-offset')
            } else {
                selectHeader?.classList.remove('fixed-top')
                nextElement?.classList.remove('scrolled-offset')
            }
        })
    }, [])
    useEffect(() => {
        let backtotop: HTMLElement | null = document.getElementById('back-to')
        window.addEventListener('scroll', (event) => {
            if (window.scrollY > 100) {
                backtotop?.classList.add('active')
            } else {
                backtotop?.classList.remove('active')
            }
        })
    }, [])

    const forMobile = () => {
        const buttonData: HTMLElement | null = document.getElementById('navbar')
        buttonData?.classList.toggle('navbar-mobile')
    }

    const logoutHandler = () => {
        signOut(auth)
            .then(() => {
                var isLogin: any = false
                dispatch(toggle_login(isLogin))

                dispatch(remove_user())
                router.replace('/login', { scroll: true })
                // Sign-out successful.
            })
            .catch((error) => {
                // An error happened.
            })
    }

    const splitPath = pathname.split('/')
    const hasUpperCase = (str: string) => /[A-Z]/.test(str)
    return (
        <>
            <header id="header" className="d-flex align-items-center shadow-md">
                <div className="container">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="logo">
                            <h1 className="text-light">
                                <Link href="/dashboard">
                                    <span
                                        style={{
                                            textTransform: 'capitalize',
                                            fontSize: '0.8em',
                                        }}
                                    >
                                        818 | Digital Agency | Admin
                                    </span>
                                </Link>
                            </h1>
                            {/* <!-- Uncomment below if you prefer to use an image logo --> */}
                            {/* <!-- <Link href="index.html"><Image src="assets/img/logo.png" alt="" className="img-fluid"></Link>--> */}
                        </div>

                        <nav id="navbar" className="navbar">
                            <ul>
                                {state.arrLinks.map((e, i) => {
                                    return (
                                        <li key={i}>
                                            <Link
                                                className={
                                                    e.linkAddress === pathname
                                                        ? 'active'
                                                        : ''
                                                }
                                                href={e.linkAddress}
                                            >
                                                {e.linkName}
                                            </Link>
                                        </li>
                                    )
                                })}

                                <li className="dropdown">
                                    <Link href="#">
                                        <span>Clients</span>{' '}
                                        <i className="bi bi-chevron-down"></i>
                                    </Link>
                                    <ul>
                                        {state.arrDropdown.map(
                                            (e, i: number) => {
                                                return (
                                                    <li key={i}>
                                                        <Link
                                                            href={e.linkAddress}
                                                        >
                                                            {e.linkName}
                                                        </Link>
                                                    </li>
                                                )
                                            }
                                        )}
                                    </ul>
                                </li>
                                <li className="dropdown">
                                    <Link href="#">
                                        <span>Others</span>{' '}
                                        <i className="bi bi-chevron-down"></i>
                                    </Link>
                                    <ul>
                                        {state.arrDropdown2.map(
                                            (e, i: number) => {
                                                return (
                                                    <li key={i}>
                                                        <Link
                                                            href={e.linkAddress}
                                                        >
                                                            {e.linkName}
                                                        </Link>
                                                    </li>
                                                )
                                            }
                                        )}
                                    </ul>
                                </li>
                                <li className="dropdown">
                                    <Link href="#">
                                        <span>{email}</span>{' '}
                                        <i className="bi bi-chevron-down"></i>
                                    </Link>
                                    <ul>
                                        <span
                                            style={{
                                                padding: '10px 20px',
                                                fontSize: '13px',
                                                textTransform: 'none',
                                                fontWeight: 500,
                                                color: '#581601',
                                                cursor: 'pointer',
                                                textAlign: 'left',
                                            }}
                                            onClick={() => logoutHandler()}
                                        >
                                            Logout
                                        </span>
                                    </ul>
                                </li>
                            </ul>
                            {/* <button onClick={(e) => forMobile(e)}>   */}
                            <i className="bi bi-list mobile-nav-toggle "></i>
                            {/* </button> */}
                        </nav>
                    </div>
                    {/* <!-- .navbar --> */}
                </div>
            </header>
            {pathname !== '/' && (
                <section id="breadcrumbs" className="breadcrumbs">
                    <div className="container">
                        <div className="d-flex justify-content-between align-items-center">
                            <>{/* <h2>About</h2> */}</>
                            <>
                                <ol>
                                    <li>
                                        <Link href="/">Home</Link>
                                    </li>
                                    <li style={{ textTransform: 'capitalize' }}>
                                        {splitPath[1] &&
                                            splitPath[1].replaceAll('-', ' ')}
                                    </li>
                                    {hasUpperCase(splitPath[2]) === true ? (
                                        <li
                                            style={{
                                                textTransform: 'capitalize',
                                            }}
                                        >
                                            Portfolio Detail
                                        </li>
                                    ) : (
                                        <li
                                            style={{
                                                textTransform: 'capitalize',
                                            }}
                                        >
                                            {splitPath[2] &&
                                                splitPath[2].replaceAll(
                                                    '-',
                                                    ' '
                                                )}
                                        </li>
                                    )}
                                </ol>
                            </>
                        </div>
                    </div>
                </section>
            )}
        </>
    )
}
export default NavAdmin
