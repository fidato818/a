import React, { useEffect, useRef, useState } from 'react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import { Base64UploadAdapter } from '@ckeditor/ckeditor5-upload'
interface CKeditorProps {
    onChange: (data: string) => void
    editorLoaded?: boolean
    name: string
    value: string
}

const Editor = ({ onChange, editorLoaded, name, value }: CKeditorProps) => {
    let [loaded, setLoaded] = useState(false)
    const editorRef = useRef<{
        CKEditor: typeof CKEditor
        ClassicEditor: typeof ClassicEditor
    }>()
    useEffect(() => {
        editorRef.current = {
            CKEditor: require('@ckeditor/ckeditor5-react').CKEditor,
            ClassicEditor: require('@ckeditor/ckeditor5-build-classic'),
        }
        setLoaded(true)
    }, [])

    // function MyCustomUploadAdapterPlugin(editor: {
    //     plugins: {
    //         get: (arg0: string) => {
    //             (): any
    //             new (): any
    //             createUploadAdapter: (loader: any) => void
    //         }
    //     }
    // }) {
    //     editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
    //         return console.log('loader', loader)
    //         //  new MyUploadAdapter(loader)
    //     }
    // }

    const uploadAdapter = (loader: { file: Promise<string | Blob> }) => {
        return {
            upload: async () => {
                //     const file = await loader.file
                //     console.log('file', file)
                //     // const base64 = await getBase64(file)
                //     // return {
                //     //     default: base64,
                //     // }
                // },
                return new Promise((resolve, reject) => {
                    const body = new FormData()
                    loader.file.then((file: string | Blob) => {
                        body.append('files', file)
                        // .then((res) => res.json())
                        // .then((res) => {
                        //     // resolve();
                        //     resolve({
                        //         default: res.imageAddress,
                        //     })
                        // })
                        // .catch((err) => {
                        //     reject(err)
                        // })

                        // fetch(uploadEndpoint, {
                        //     method: 'POST',
                        //     headers: {
                        //         Authorization: `Bearer ${token}`,
                        //     },
                        //     body: body,
                        // })
                        //     .then((res) => res.json())
                        //     .then((res) => {
                        //         // resolve();
                        //         resolve({
                        //             default: res.imageAddress,
                        //         })
                        //     })
                        //     .catch((err) => {
                        //         reject(err)
                        //     })
                    })
                })
            },
        }
    }
    function uploadPlugin(editor: {
        plugins: {
            get: (arg0: string) => {
                (): any
                new (): any
                createUploadAdapter: (loader: any) => {
                    upload: () => Promise<unknown>
                }
            }
        }
    }) {
        editor.plugins.get('FileRepository').createUploadAdapter = (loader: {
            file: Promise<string | Blob>
        }) => {
            return uploadAdapter(loader)
        }
    }

    return (
        <CKEditor
            // onInit={(editor: {
            //     editing: {
            //         view: {
            //             change: (arg0: (writer: any) => void) => void
            //             document: { getRoot: () => any }
            //         }
            //     }
            // }) => {
            //     // You can store the "editor" and use when it is needed.
            //     // console.log("Editor is ready to use!", editor);
            //     return (
            //         // You can store the "editor" and use when it is needed.
            //         // console.log("Editor is ready to use!", editor);
            //         // You can store the "editor" and use when it is needed.
            //         // console.log("Editor is ready to use!", editor);
            //         // You can store the "editor" and use when it is needed.
            //         // console.log("Editor is ready to use!", editor);
            //         // You can store the "editor" and use when it is needed.
            //         // console.log("Editor is ready to use!", editor);
            //         // You can store the "editor" and use when it is needed.
            //         // console.log("Editor is ready to use!", editor);
            //         editor.editing.view.change((writer) => {
            //             writer.setStyle(
            //                 'height',
            //                 '200px',
            //                 editor.editing.view.document.getRoot()
            //             )
            //         })
            //     )
            // }}
            // config={{ plugins: [Base64UploadAdapter] }}
            // config={{
            //     extraPlugins: [uploadPlugin],
            // }}
            // onInit={(editor: React.SetStateAction<null>) => setEditor(editor)}
            // style={{ height: '25rem' }}
            editor={ClassicEditor}
            // config={{
            //     heading: {
            //         // options: [
            //         //     {
            //         //         model: 'paragraph',
            //         //         title: 'Paragraph',
            //         //         className:
            //         //             'ck-heading_paragraph',
            //         //     },
            //         //     {
            //         //         model: 'heading1',
            //         //         view: 'h1',
            //         //         title: 'Heading 1',
            //         //         className:
            //         //             'ck-heading_heading1',
            //         //     },
            //         //     {
            //         //         model: 'heading2',
            //         //         view: 'h2',
            //         //         title: 'Heading 2',
            //         //         className:
            //         //             'ck-heading_heading2',
            //         //     },
            //         //     {
            //         //         model: 'heading3',
            //         //         view: 'h3',
            //         //         title: 'Heading 3',
            //         //         className:
            //         //             'ck-heading_heading3',
            //         //     },
            //         //     {
            //         //         model: 'heading4',
            //         //         view: 'h4',
            //         //         title: 'Heading 4',
            //         //         className:
            //         //             'ck-heading_heading4',
            //         //     },
            //         //     {
            //         //         model: 'heading5',
            //         //         view: 'h5',
            //         //         title: 'Heading 5',
            //         //         className:
            //         //             'ck-heading_heading5',
            //         //     },
            //         //     {
            //         //         model: 'heading6',
            //         //         view: 'h6',
            //         //         title: 'Heading 6',
            //         //         className:
            //         //             'ck-heading_heading6',
            //         //     },
            //         // ],
            //     },
            //     // plugins: [Paragraph, Bold, Italic, Essentials],
            //     toolbar: [
            //         'paragraph',
            //         'heading1',
            //         'heading2',
            //         'heading3',
            //         'heading4',
            //         'heading5',
            //         'heading6',

            //         'undo',
            //         'redo',
            //         'heading',
            //         '|',
            //         'bold',
            //         'italic',
            //         'link',
            //         'bulletedList',
            //         'numberedList',
            //         'blockQuote',
            //         // "bold",
            //         // "italic",
            //     ],
            // }}
            data={value}
            // onReady={(editor) => {
            //     console.log('Editor is ready to use!', editor)
            // }}
            onChange={(event: any, editor: any) => {
                const data = editor.getData()
                // console.log('data', data)
                // setState({ body: data })
                onChange(data)
            }}
            // onBlur={(event, editor) => {
            //     console.log('Blur.', editor)
            // }}
            // onFocus={(event, editor) => {
            //     console.log('Focus.', editor)
            // }}
        />
    )
}

export default Editor

// <CKEditor
//     editor={ClassicEditor}
//     data={value}
//     onChange={(event, editor) => {
//         const data = editor.getData()
//         onChange(data)
//     }}
// />
