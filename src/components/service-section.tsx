'use client'
import React from 'react'
import Image from 'next/image'

interface ArrayService {
    imageUrl: string
    width: number
    height: number
}

const ServiceComponent = () => {
    return (
        <section id="services" className="services">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6">
                        <div className="icon-box" data-aos="fade-up">
                            <div className="icon">
                                <i className="bi bi-laptop"></i>
                            </div>

                            <h4 className="title">
                                <a href="">Web & Mobile App Design</a>
                            </h4>
                            <p className="description">
                                Performing & High Usability Design and
                                Interfaces. We should be on the path the a
                                functional design so now for the exciting bit,
                                beautiful, clean user interface.
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                        <div
                            className="icon-box"
                            data-aos="fade-up"
                            data-aos-delay="100"
                        >
                            <div className="icon">
                                <i className="bi bi-card-checklist"></i>
                            </div>
                            <h4 className="title">
                                <a href="">Web App Development</a>
                            </h4>
                            <p className="description">
                                Every website should be built with two primary
                                goals: Firstly, it needs to work across all
                                devices. Secondly, it needs to be fast as
                                possible.
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                        <div
                            className="icon-box"
                            data-aos="fade-up"
                            data-aos-delay="200"
                        >
                            <div className="icon">
                                <i className="bi bi-phone"></i>
                            </div>
                            <h4 className="title">
                                <a href="">Mobile App Development</a>
                            </h4>
                            <p className="description">
                                Develop unique and fully customized mobile
                                applications for an optimal user experience that
                                achieves your business goals.
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                        <div
                            className="icon-box"
                            data-aos="fade-up"
                            data-aos-delay="200"
                        >
                            <div className="icon">
                                {/* <i className="bi bi-binoculars"></i> */}
                                <Image
                                    src={require('@/assets/images/pwa-svgrepo-com.svg')}
                                    alt=""
                                    width={70}
                                    height={40}
                                />
                            </div>
                            <h4 className="title">
                                <a href="">Progressive Web Apps</a>
                            </h4>
                            <p className="description">
                                Progressive Web pages enable you to connect to
                                the website, even when you are offline, PWA’s
                                are not platform dependent and can run on any OS
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                        <div
                            className="icon-box"
                            data-aos="fade-up"
                            data-aos-delay="300"
                        >
                            <div className="icon">
                                <i className="bi bi-pc-display-horizontal"></i>
                            </div>
                            <h4 className="title">
                                <a href="">Point Of Sale</a>
                            </h4>
                            <p className="description">
                                Point of sale (POS) is a system for processing
                                sales transactions, managing inventory, and
                                tracking customer data in retail businesses.
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                        <div
                            className="icon-box"
                            data-aos="fade-up"
                            data-aos-delay="400"
                        >
                            <div className="icon">
                                <i className="bi bi-shop"></i>
                            </div>
                            <h4 className="title">
                                <a href="">E-Commerce Web & Mobile</a>
                            </h4>
                            <p className="description">
                                Seamless shopping experiences on web and mobile
                                platforms for easy online product browsing,
                                purchasing, and payment.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ServiceComponent
