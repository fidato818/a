import { initializeApp, getApps } from 'firebase/app'
import { getMessaging, isSupported } from 'firebase/messaging'

import { getAnalytics } from 'firebase/analytics'
import { getDatabase } from 'firebase/database'
import { getAuth } from 'firebase/auth'

function initializeAppIfNecessary() {
    try {
        return getApp()
    } catch (any) {
        const firebaseConfig = {
            // apiKey: process.env.NEXT_PUBLIC_REACT_APP_apiKey,
            // authDomain: process.env.NEXT_PUBLIC_REACT_APP_authDomain,
            // databaseURL: process.env.NEXT_PUBLIC_REACT_APP_databaseURL,
            // projectId: process.env.NEXT_PUBLIC_REACT_APP_projectId,
            // storageBucket: process.env.NEXT_PUBLIC_REACT_APP_storageBucket,
            // messagingSenderId: process.env.NEXT_PUBLIC_REACT_APP_messagingSenderId,
            // appId: process.env.NEXT_PUBLIC_REACT_APP_appId,
            // measurementId: process.env.NEXT_PUBLIC_REACT_APP_measurementId,
            // apiKey: "AIzaSyAKWjcz-LEED_-LcTSd3ZCkemGwIfcMC8A",
            // authDomain: "react-rnchatapp.firebaseapp.com",
            // databaseURL: "https://react-rnchatapp.firebaseio.com",
            // projectId: "react-rnchatapp",
            // storageBucket: "react-rnchatapp.appspot.com",
            // messagingSenderId: "512109718841",
            // appId: "1:512109718841:web:976a6a2ee180a417"

            apiKey: 'AIzaSyBgWalza_zvr3jnpyTkoK3UMwiFG1hV-zY',
            authDomain: 'adnan-ahmed.firebaseapp.com',
            databaseURL: 'https://adnan-ahmed.firebaseio.com',
            projectId: 'adnan-ahmed',
            storageBucket: 'adnan-ahmed.appspot.com',
            messagingSenderId: '522432240253',
            appId: '1:522432240253:web:4344d443c8eead43',
            measurementId: 'G-ZW03GDM9VS',
        }
        return initializeApp(firebaseConfig)
    }
}

export const app = initializeAppIfNecessary()
const initAnalytics = (args) => {
    if (typeof window !== 'undefined') {
        return getAnalytics(args)
    } else {
        return null
    }
}
export const analytics = initAnalytics(app)
export const db = getDatabase(app)
export const auth = getAuth(app)

export const messaging = async () => (await isSupported()) && getMessaging(app)

// export const messaging = getMessaging(app);
// export const onMessageListener = () =>
//   new Promise((resolve) => {
//     onMessage(messaging, (payload) => {
//       resolve(payload);
//     });
//   });
