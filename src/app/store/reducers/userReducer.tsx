import { createSlice, current, PayloadAction } from '@reduxjs/toolkit'

// Define a type for the slice state
interface UserState {
    value: []
    user: string
    isLogin: string
}

const initialState: UserState = {
    value: [],
    user: '',
    isLogin: '',
}

export const userSlice = createSlice({
    name: 'User_to_Redux',
    initialState,

    reducers: {
        update_user: (state, action: PayloadAction<string>) => {
            return { ...state, user: action.payload }
        },
        remove_user: (state) => {
            return { ...state, user: '' }
        },
        toggle_login: (state, action: PayloadAction<string>) => {
            return { ...state, isLogin: action.payload }
        },
    },
})

// Action creators are generated for each case reducer function
export const { update_user, remove_user, toggle_login } = userSlice.actions

export default userSlice.reducer
