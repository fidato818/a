'use client'
import './globals.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'boxicons/css/boxicons.min.css'
import 'aos/dist/aos.css'
import AOS from 'aos'


import { Providers } from './store/provider'
import { useRouter, usePathname } from 'next/navigation'
import { useEffect, useState } from 'react'
import ProtectedRoute from '../components/withPrivateRoute'
import { Outfit } from 'next/font/google'
 
const roboto = Outfit({
  weight: '400',
  subsets: ['latin'],
  display: 'swap',
})

const RootLayout = ({ children }: { children: React.ReactNode }) => {
    const [isClient, setIsClient] = useState(false)
    const pathname = usePathname()
    useEffect(() => {
        AOS.init({
            duration: 1000,
            easing: 'ease-in-out',
            once: true,
            mirror: false,
        })
    }, [])

    useEffect(() => {
        setIsClient(true)
    }, [])

    const pathSplit = pathname.split('/')
    const noAuthRequired = [
        '/',
        '/about',
        '/contact',
        '/pages/e-commerce',
        '/pages/mobile-app-development',
        `/portfolio/${pathSplit[2]}`,
        '/pages/point-of-sale',
        '/pages/web-development',
        '/portfolio',
        '/project-planner',
        '/services',
        '/testimonials',
        '/login',
        '/signup',
        '/blogs',
        `/blogs/${pathSplit[2]}`,
        `/client-projects/${pathSplit[2]}`,
        '/update-password',
    ]

    // const allRoutes = [...noAuthRequired, ...villains]
    return (
        <Providers>
            <html lang="en">
                {/* <body className={inter.className}> */}
                <body className={roboto.className}>
                    {noAuthRequired.includes(pathname) ? (
                        <div>{children}</div>
                    ) : (
                        <>
                            {isClient && (
                                <ProtectedRoute>
                                    <div>{children}</div>
                                </ProtectedRoute>
                            )}
                        </>
                    )}
                </body>
            </html>
        </Providers>
    )
}
export default RootLayout
