// import Ticket from '../ticket'

// export async function generateStaticParams() {
//     const posts = await fetch(
//         'https://adnan-ahmed.firebaseio.com/Projects.json?print=pretty'
//     )
//         .then((res) => res.json())
//         .then((e) => Object.values(e))

//     return posts.map((post: any) => ({
//         id: post?.newPostKey,
//     }))
// }

// export default async function Page({ params }: { params: { id: string } }) {
//     return <Ticket params={{ id: params.id }} />
// }
import Ticket from '../ticket'
import axios from 'axios'
let trendingTracks: any[] = []
export async function generateStaticParams() {
    try {
        const response = await axios.get(
            'https://adnan-ahmed.firebaseio.com/Projects.json?print=pretty'
        )

        if (response.data) {
            trendingTracks = []
            for (var item of Object.values(response.data)) {
                trendingTracks.push(item)
            }
        }
    } catch (error) {
        console.error(error)
    }

    return trendingTracks.map((post: any) => ({
        id: post.newPostKey,
    }))
}

export default function Page({ params }: { params: { id: string } }) {
    return <Ticket params={{ id: params.id }} />
}
