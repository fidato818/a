"use client"
import React from 'react'
import { useSetState } from 'ahooks'
import Image from 'next/image'
interface arraySvg {
    arrSvg: { imageLink: string, altName: string, heightImage: number, widthImage: number }[]
}

const MOBILEPAGE = () => {
    const [state, setState] = useSetState<arraySvg>(
        {
            arrSvg: [

                { imageLink: require('@/assets/images/icons8-material-ui.svg'), altName: "material-ui", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-bootstrap.svg'), altName: "bootstrap", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-wordpress.svg'), altName: "wordpress", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-tailwindcss.svg'), altName: "tailwindcss", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-css.svg'), altName: "css", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/materialize.svg'), altName: "css", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-html.svg'), altName: "html", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-nextjs.svg'), altName: "nextjs", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-react.svg'), altName: "react", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/pwa-svgrepo-com.svg'), altName: "pwa-svgrepo-com", heightImage: 100, widthImage: 100 }
            ]
        }
    )

    return (
        <div className='container'>
            <h1 className='mt-4'>Services</h1>
            <h5 className='mb-4'>We specialize in cutting-edge web app development, where innovation meets functionality. Our dedicated team of experienced developers and designers collaborates with clients to create tailored web applications that solve unique business challenges and engage users.</h5>
            <section id="services" className="services">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6">
                            <div className="icon-box" data-aos="fade-up">
                                <div className="icon">
                                    <i className="bi bi-laptop"></i>
                                </div>

                                <h4 className="title">
                                    <a href="">Cutting-Edge Technologies</a>
                                </h4>
                                <p className="description">
                                    We stay at the forefront of web development technologies and trends. From responsive design for mobile-friendliness to the latest frameworks and languages, we leverage the best tools for your project.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="100"
                            >
                                <div className="icon">
                                    <i className="bi bi-bar-chart-line-fill"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">Scalability and Performance</a>
                                </h4>
                                <p className="description">
                                    Our web apps are built with scalability in mind, ensuring they can grow with your business. We optimize performance for fast loading times and seamless user interactions.
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="200"
                            >
                                <div className="icon">
                                    <i className="bi bi-shield-check"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">Security and Compliance</a>
                                </h4>
                                <p className="description">
                                    We prioritize the security of your data and user information. Our development process includes robust security measures and compliance with industry standards and regulations.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="300"
                            >
                                <div className="icon">
                                    <i className="bi bi-list-check"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">Testing and Quality Assurance</a>
                                </h4>
                                <p className="description">
                                    Rigorous testing ensures that your web app functions flawlessly. We identify and address any issues before launch to deliver a smooth user experience.
                                </p>
                            </div>
                        </div>
                        <h1 className='mt-4'>Our  <strong>Expertise</strong></h1>

                        <div className="row">
                            {state.arrSvg.map((e, i) => {
                                return (
                                    <div className="col-lg-3 col-md-6" key={i}>
                                        <div
                                            className="icon-box-1"
                                            data-aos="fade-up"
                                            data-aos-delay="300">
                                            <Image
                                                src={e.imageLink}
                                                alt={e.altName}
                                                width={e.heightImage}
                                                height={e.widthImage} />
                                        </div>
                                    </div>
                                )
                            })}


                        </div>
                        {/* </div> */}
                    </div>
                </div>
            </section>
        </div>
    )
}

export default MOBILEPAGE