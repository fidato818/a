'use client'
import emailjs from '@emailjs/browser'
import { useSetState } from 'ahooks'

const Contact = () => {
    const [state, setState] = useSetState({
        clickedLoader: false,
       
        showText: false,
        clientName: '',
        clientEmail: '',
        projectDetail: '',
        primaryObjective: '',
    })
    const changeHandler = (
        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
        event.preventDefault()
        const { name, value } = event.target
        let obj: any = { [name]: value }
        setState(obj)
    }

    const sendEmailtoMe = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        setState({ clickedLoader: true, })
        const { clientName, clientEmail, projectDetail, primaryObjective } =
            state

        const templateParams = {
            full_name: clientName,
            from_email: clientEmail,
            create_at: new Date().toLocaleString(),
            message: `
                    Data From Contact Us

                    Client Name: ${clientName} 

                    Client Email: ${clientEmail}               

                    Subject: ${projectDetail}                     

                    Message: ${primaryObjective}             
                    `,
        }
        // console.log('templateParams', templateParams)
        emailjs
            .send(
                'gmail_edell5400',
                'template_pa2iisv',
                templateParams,
                '5I3ybAUOdfGjRASHh'
            )
            .then(
                (response) => {
                    console.log('SUCCESS!')
                    setState({                      
                        clickedLoader: false,
                        clientName: '',
                        clientEmail: '',
                        projectDetail: '',
                        primaryObjective: '',
                        showText: true,
                    })
                    // setTimeout(() => {
                    //     setState({ showText: true })
                    // }, 500)
                },
                (err) => {
                    console.log('FAILED...', err)
                }
            )
    }
    // const handleClick = () => {
    //     setState({ clickedLoader: true })
    // }
    const { clientName, clientEmail, projectDetail, primaryObjective } = state
    return (
        <main>
            <div className="map-section">
                <iframe
                    style={{ border: 0, width: '100%', height: '350px' }}
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
                    frameBorder={0}
                    allowFullScreen
                ></iframe>
            </div>

            <section id="contact" className="contact">
                <div className="container">
                    <div
                        className="row justify-content-center"
                        data-aos="fade-up"
                    >
                        <div className="col-lg-10">
                            <div className="info-wrap">
                                <div className="row">
                                    <div className="col-lg-4 info">
                                        <i className="bi bi-geo-alt"></i>
                                        <h4>Location:</h4>
                                        <p>
                                            A108 Adam Street
                                            <br />
                                            New York, NY 535022
                                        </p>
                                    </div>

                                    <div className="col-lg-4 info mt-4 mt-lg-0">
                                        <i className="bi bi-envelope"></i>
                                        <h4>Email:</h4>
                                        <p>
                                            adnanahmed818@gmail.com
                                            {/* <br />contact@example.com */}
                                        </p>
                                    </div>

                                    <div className="col-lg-4 info mt-4 mt-lg-0">
                                        <i className="bi bi-phone"></i>
                                        <h4>Call:</h4>
                                        <p>
                                            +92 345 304 8496
                                            <br />
                                            +92 312 254 9020
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div
                        className="row mt-5 justify-content-center"
                        data-aos="fade-up"
                    >
                        <div className="col-lg-10">
                            <form
                                // action="forms/contact.php"
                                onSubmit={sendEmailtoMe}
                                method="post"
                                role="form"
                                className="php-email-form"
                            >
                                <div className="row">
                                    <div className="col-md-6 form-group">
                                        <input
                                            type="text"
                                            name="clientName"
                                            className="form-control"
                                            id="clientName"
                                            value={clientName}
                                            placeholder="Your Name"
                                            required
                                            onChange={changeHandler}
                                        />
                                    </div>
                                    <div className="col-md-6 form-group mt-3 mt-md-0">
                                        <input
                                            type="email"
                                            className="form-control"
                                            name="clientEmail"
                                            id="clientEmail"
                                            value={clientEmail}
                                            placeholder="Your Email"
                                            required
                                            onChange={changeHandler}
                                        />
                                    </div>
                                </div>
                                <div className="form-group mt-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="projectDetail"
                                        value={projectDetail}
                                        id="projectDetail"
                                        placeholder="Subject"
                                        required
                                        onChange={changeHandler}
                                    />
                                </div>
                                <div className="form-group mt-3">
                                    <textarea
                                        className="form-control"
                                        name="primaryObjective"
                                        value={primaryObjective}
                                        rows={5}
                                        placeholder="Message"
                                        required
                                        onChange={changeHandler}
                                    ></textarea>
                                </div>
                                <div className="my-3">
                                    <div
                                        className={
                                            state.clickedLoader
                                                ? 'loading d-block'
                                                : 'loading'
                                        }
                                    >
                                        Loading
                                    </div>
                                    {/* <div className="error-message d-block"></div> */}
                                    <div
                                        className={
                                            state.showText
                                                ? 'sent-message d-block'
                                                : 'sent-message'
                                        }
                                    >
                                        Your message has been sent. Thank you!
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button
                                        disabled={state.clickedLoader ? true : false}
                                        type="submit"
                                    >
                                        Send Message
                                    </button>
                                   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
}
export default Contact
