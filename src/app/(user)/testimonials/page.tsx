'use client'

import { useSetState } from 'ahooks'
import { onValue, equalTo, ref, query, orderByChild } from 'firebase/database'
import { Parser } from 'html-to-react'
import Link from 'next/link'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import { db } from '../../../config/firebaseConfig'
// import {}
import { useEffect } from 'react'
interface sample_arr {
    arrTestimonials: {
        body: string
        buttonLink: string
        category: string
        createAt: string
        currentUser: string
        downloadURL: string
        githubLink: string
        headingName: string
        newPostKey: string
        status: boolean
        updateAt: string
        redirectLink: string
    }[]
}

const Testimonials = () => {
    const [state, setState] = useSetState<sample_arr>({
        arrTestimonials: [],
    })

    useEffect(() => {
        // const starCountRef = ref(db, 'clientTestimonials')
        const starCountRef = query(
            ref(db, 'clientTestimonials'),
            orderByChild('status'),
            equalTo(true)
        )
        var arr: any[] = []
        onValue(starCountRef, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                var childData = childSnapshot.val()

                arr.push(childData)
            })
            setState({ arrTestimonials: arr })
        })
    }, [setState])

    const { arrTestimonials } = state
    return (
        <div className="container">
            <div className="mt-4">
                <h1 data-aos="fade-up" className="text-center ">
                    Testimonials
                </h1>
                <h6 data-aos="fade-up" className="text-center">
                    There&apos;s nothing more gratifying than receiving glowing
                    reviews from satisfied clients who not only recommend our
                    services but also entrust us with their business.
                </h6>
            </div>
            <section id="testimonials" className="testimonials">
                <div className="row">
                    {arrTestimonials.map((e, i) => {
                        return (
                            <div
                                className="col-lg-6 mb-3"
                                data-aos="fade-up"
                                key={i}
                            >
                                <div className="testimonial-item">
                                    {/* <Image src={e.downloadURL} className="testimonial-img" alt=""
                                            // layout='fill'
                                            //  objectFit='cover' 
                                            // objectPosition='center'
                                            width={100}
                                            height={80}
                                        /> */}

                                    <div
                                        style={{
                                            display: 'flex',
                                            justifyContent: 'space-between',
                                        }}
                                    >
                                        <div>
                                            <h3>{e.headingName}</h3>
                                        </div>
                                        <div>
                                            <OverlayTrigger
                                                overlay={
                                                    <Tooltip id="tooltip-disabled">
                                                        User Review Link
                                                    </Tooltip>
                                                }
                                            >
                                                <span className="d-inline-block">
                                                    {e.redirectLink !==
                                                        undefined && (
                                                        <Link
                                                            target="_blank"
                                                            href={
                                                                e.redirectLink
                                                                    ? e.redirectLink
                                                                    : '#'
                                                            }
                                                        >
                                                            <i className="bi bi-box-arrow-up-right" style={{color: '#7666df'}}></i>
                                                        </Link>
                                                    )}
                                                </span>
                                            </OverlayTrigger>
                                            <span
                                                title="Header"
                                                data-bs-toggle="popover"
                                                data-bs-trigger="hover"
                                                data-bs-content="Popover text"
                                            ></span>
                                        </div>
                                    </div>
                                    <h4>
                                        {e.githubLink} | {e.buttonLink}
                                    </h4>

                                    <p>
                                        {/* <i className="bx bxs-quote-alt-left quote-icon-left"></i> */}
                                        {/* <i className="bx bxs-quote-alt-left quote-icon-left"></i>{' '} */}
                                        {Parser().parse(e.body)}{' '}
                                        {/* <i className="bx bxs-quote-alt-right quote-icon-right"></i> */}
                                        {/* Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper. */}
                                        {/* <i className="bx bxs-quote-alt-right quote-icon-right"></i> */}
                                    </p>
                                </div>
                            </div>
                        )
                    })}
                    {/* <div className="col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div className="testimonial-item mt-4 mt-lg-0">
                                <img src="assets/img/testimonials/testimonials-2.jpg" className="testimonial-img" alt="" />
                                <h3>Sara Wilsson</h3>
                                <h4>Designer</h4>
                                <p>
                                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6" data-aos="fade-up" data-aos-delay="200">
                            <div className="testimonial-item mt-4">
                                <img src="assets/img/testimonials/testimonials-3.jpg" className="testimonial-img" alt="" />
                                <h3>Jena Karlis</h3>
                                <h4>Store Owner</h4>
                                <p>
                                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6" data-aos="fade-up" data-aos-delay="300">
                            <div className="testimonial-item mt-4">
                                <img src="assets/img/testimonials/testimonials-4.jpg" className="testimonial-img" alt="" />
                                <h3>Matt Brandon</h3>
                                <h4>Freelancer</h4>
                                <p>
                                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6" data-aos="fade-up" data-aos-delay="400">
                            <div className="testimonial-item mt-4">
                                <img src="assets/img/testimonials/testimonials-5.jpg" className="testimonial-img" alt="" />
                                <h3>John Larson</h3>
                                <h4>Entrepreneur</h4>
                                <p>
                                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6" data-aos="fade-up" data-aos-delay="500">
                            <div className="testimonial-item mt-4">
                                <img src="assets/img/testimonials/testimonials-6.jpg" className="testimonial-img" alt="" />
                                <h3>Emily Harison</h3>
                                <h4>Store Owner</h4>
                                <p>
                                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Eius ipsam praesentium dolor quaerat inventore rerum odio. Quos laudantium adipisci eius. Accusamus qui iste cupiditate sed temporibus est aspernatur. Sequi officiis ea et quia quidem.
                                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div> */}
                </div>
            </section>
        </div>
    )
}
export default Testimonials
