'use client'

import { useSetState } from 'ahooks'
import { ref, onValue } from 'firebase/database'
import { Parser } from 'html-to-react'
import Image from 'next/image'
import Link from 'next/link'
import moment from 'moment'

import { db } from '@/config/firebaseConfig'
import { useEffect } from 'react'
import RecentPosts from '@/components/recent-posts'
const BlogDetail = ({ params }: { params: { id: string } }) => {
    const [state, setState] = useSetState<any | null>({
        body: '',
        buttonLink: '',
        category: '',
        downloadURL: '',
        githubLink: '',
        headingName: '',
        updateAt: '',
        clientWebsite: '',
        createAt: '',
        arrTech: [],
        arr: [],
        startDate: new Date(),
        endDate: new Date(),
    })

    useEffect(() => {
        // props.callFunc()
        const getData = () => {
            const { id } = params
            const starCountRef = ref(db, 'blogsData/' + id)
            onValue(starCountRef, (snapshot) => {
                var childData = snapshot.val()
                const {
                    body,
                    buttonLink,
                    category,
                    githubLink,
                    downloadURL,
                    headingName,
                    clientWebsite,
                    createAt,
                    arrTech,
                    startDate,
                    endDate,
                } = childData
                // console.log('childData', childData)

                setState({
                    body,
                    buttonLink,
                    category,
                    githubLink,
                    downloadURL,
                    headingName,
                    clientWebsite,
                    createAt,
                    arrTech,
                    startDate,
                    endDate,
                })
            })
        }
        getData()
    }, [params, setState])

    const {
        body,
        category,
        downloadURL,
        headingName,
        clientWebsite,
        createAt,
        startDate,
        endDate,
        buttonLink,
    } = state
    const splitCategory = category.split('-')
    // console.log(splitCategory)
    return (
        <main>
            <section id="blog" className="blog">
                <div className="container" data-aos="fade-up">
                    <div className="row">
                        <div className="col-lg-8 entries">
                            <article className="entry entry-single">
                                <div className="entry-img">
                                    <Image
                                        src={downloadURL}
                                        alt=""
                                        className="img-fluid"
                                        width={100}
                                        height={100}
                                        style={{
                                            width: '100%',
                                            height: 'auto',
                                        }}
                                    />
                                </div>

                                <h2 className="entry-title">
                                    <Link href="#">{headingName}</Link>
                                </h2>

                                <div className="entry-meta">
                                    <ul>
                                        {/* <li className="d-flex align-items-center"><i className="bi bi-person"></i> <Link href="blog-single.html">John Doe</a></li>
                                        <li className="d-flex align-items-center"><i className="bi bi-clock"></i> <Link href="blog-single.html">
                                            <time datetime="2020-01-01">Jan 1, 2020</time>
                                        </a></li> */}
                                        <li className="d-flex align-items-center">
                                            <i className="bi bi-person"></i>{' '}
                                            <Link href="blog-single.html">
                                                {buttonLink}
                                            </Link>
                                        </li>
                                        <li className="d-flex align-items-center">
                                            <i className="bi bi-clock"></i>{' '}
                                            <Link href="blog-single.html">
                                                <time dateTime="2020-01-01">
                                                    {/* Jan 1, 2020 */}
                                                    {/* {e.createAt} */}
                                                    {moment(createAt).format(
                                                        'MMM D, YYYY'
                                                    )}
                                                </time>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>

                                <div className="entry-content">
                                    <p>{Parser().parse(body)}</p>

                                    {/* <p>
                                        Sit repellat hic cupiditate hic ut nemo. Quis nihil sunt non reiciendis. Sequi in accusamus harum vel aspernatur. Excepturi numquam nihil cumque odio. Et voluptate cupiditate.
                                    </p>

                                    <blockquote>
                                        <p>
                                            Et vero doloremque tempore voluptatem ratione vel aut. Deleniti sunt animi aut. Aut eos aliquam doloribus minus autem quos.
                                        </p>
                                    </blockquote>

                                    <p>
                                        Sed quo laboriosam qui architecto. Occaecati repellendus omnis dicta inventore tempore provident voluptas mollitia aliquid. Id repellendus quia. Asperiores nihil magni dicta est suscipit perspiciatis. Voluptate ex rerum assumenda dolores nihil quaerat.
                                        Dolor porro tempora et quibusdam voluptas. Beatae aut at ad qui tempore corrupti velit quisquam rerum. Omnis dolorum exercitationem harum qui qui blanditiis neque.
                                        Iusto autem itaque. Repudiandae hic quae aspernatur ea neque qui. Architecto voluptatem magni. Vel magnam quod et tempora deleniti error rerum nihil tempora.
                                    </p>

                                    <h3>Et quae iure vel ut odit alias.</h3>
                                    <p>
                                        Officiis animi maxime nulla quo et harum eum quis a. Sit hic in qui quos fugit ut rerum atque. Optio provident dolores atque voluptatem rem excepturi molestiae qui. Voluptatem laborum omnis ullam quibusdam perspiciatis nulla nostrum. Voluptatum est libero eum nesciunt aliquid qui.
                                        Quia et suscipit non sequi. Maxime sed odit. Beatae nesciunt nesciunt accusamus quia aut ratione aspernatur dolor. Sint harum eveniet dicta exercitationem minima. Exercitationem omnis asperiores natus aperiam dolor consequatur id ex sed. Quibusdam rerum dolores sint consequatur quidem ea.
                                        Beatae minima sunt libero soluta sapiente in rem assumenda. Et qui odit voluptatem. Cum quibusdam voluptatem voluptatem accusamus mollitia aut atque aut.
                                    </p>
                                    <img src="assets/img/blog/blog-inside-post.jpg" className="img-fluid" alt="" />

                                    <h3>Ut repellat blanditiis est dolore sunt dolorum quae.</h3>
                                    <p>
                                        Rerum ea est assumenda pariatur quasi et quam. Facilis nam porro amet nostrum. In assumenda quia quae a id praesentium. Quos deleniti libero sed occaecati aut porro autem. Consectetur sed excepturi sint non placeat quia repellat incidunt labore. Autem facilis hic dolorum dolores vel.
                                        Consectetur quasi id et optio praesentium aut asperiores eaque aut. Explicabo omnis quibusdam esse. Ex libero illum iusto totam et ut aut blanditiis. Veritatis numquam ut illum ut a quam vitae.
                                    </p>
                                    <p>
                                        Alias quia non aliquid. Eos et ea velit. Voluptatem maxime enim omnis ipsa voluptas incidunt. Nulla sit eaque mollitia nisi asperiores est veniam.
                                    </p> */}
                                </div>

                                {/* <div className="entry-footer">
                                    <i className="bi bi-folder"></i>
                                    <ul className="cats">
                                        <li><Link href="#">Business</a></li>
                                    </ul>

                                    <i className="bi bi-tags"></i>
                                    <ul className="tags">
                                        <li><Link href="#">Creative</a></li>
                                        <li><Link href="#">Tips</a></li> 
                                        <li><Link href="#">Marketing</a></li>
                                    </ul>
                                </div> */}
                            </article>

                            <div
                                className="blog-author d-flex align-items-center"
                                style={{
                                    padding: '20px ',
                                    marginBottom: '30px',
                                    boxShadow: '0 4px 16px rgba(0, 0, 0, 0.1)',
                                }}
                            >
                                {/* <Image
                                    style={{
                                        width: '150px',
                                        height: '150px',
                                        marginRight: '20px',
                                    }}
                                    src={require('@/assets/images/102.jpg')}
                                    className="rounded-circle float-left"
                                    alt=""
                                /> */}
                                <div>
                                    <h4>Adnan Ahmed</h4>
                                    <div
                                        className="social-links"
                                        style={{ margin: '6px 0px' }}
                                    >
                                        <Link href="https://twitter.com/AdnanAhmed_818">
                                            <i className="bi bi-twitter-x"></i>
                                        </Link>
                                        <Link href="https://facebook.com/millions818">
                                            <i className="bi bi-facebook"></i>
                                        </Link>
                                        <Link href="https://instagram.com/adnan_ahmed818">
                                            <i className="biu bi-instagram"></i>
                                        </Link>
                                        {/* <Link href="https://instagram.com/#">
                                            <i className="bi bi-linkedin"></i>
                                        </Link>
                                        <Link href="https://instagram.com/#">
                                            <i className="bi bi-youtube"></i>
                                        </Link>
                                        <Link href="https://instagram.com/#">
                                            <i className="bi bi-github"></i>
                                        </Link> */}
                                    </div>
                                    <p
                                        style={{
                                            color: '#000000b0',
                                            fontStyle: 'normal',
                                        }}
                                    >
                                        Professional Mobile App Developer
                                        specializing in React Native and Expo,
                                        dedicated to crafting exceptional user
                                        experiences. With a robust skill set
                                        encompassing Firebase 🌐 (Database,
                                        Authentication, Hosting, Storage),
                                        Google Maps 🗺️ integration, and
                                        expertise in building Food Delivery Apps
                                        🍔🛵 similar to Food Panda and Uber
                                        Eats, I bring a holistic approach to app
                                        development.
                                    </p>
                                </div>
                            </div>
                            {/* <div className="blog-author d-flex align-items-center">
                                <Image
                                    src={require('@/assets/images/-preview-2.png')}
                                    className="rounded-circle float-left"
                                    alt=""
                                />
                                <div>
                                    <h4>Jane Smith</h4>
                                    <div className="social-links">
                                        <Link href="https://twitters.com/#">
                                            <i className="bi bi-twitter"></i>
                                        </a>
                                        <Link href="https://facebook.com/#">
                                            <i className="bi bi-facebook"></i>
                                        </a>
                                        <Link href="https://instagram.com/#">
                                            <i className="biu bi-instagram"></i>
                                        </a>
                                    </div>
                                    <p>
                                        Itaque quidem optio quia voluptatibus
                                        dolorem dolor. Modi eum sed possimus
                                        accusantium. Quas repellat voluptatem
                                        officia numquam sint aspernatur
                                        voluptas. Esse et accusantium ut unde
                                        voluptas.
                                    </p>
                                </div>
                            </div> */}
                        </div>
                        {/* <!-- End blog entries list --> */}

                        <div className="col-lg-4">
                            <div className="sidebar">
                                {/* <h3 className="sidebar-title">Search</h3>
                                <div className="sidebar-item search-form">
                                    <form action="">
                                        <input type="text" />
                                        <button type="submit">
                                            <i className="bi bi-search"></i>
                                        </button>
                                    </form>
                                </div> */}

                                <h3 className="sidebar-title">Categories</h3>
                                <div className="sidebar-item categories">
                                    <ul>
                                        <li>
                                            <Link href="#">
                                                General <span>(25)</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="#">
                                                Lifestyle <span>(12)</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="#">
                                                Travel <span>(5)</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="#">
                                                Design <span>(22)</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="#">
                                                Creative <span>(8)</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="#">
                                                Education <span>(14)</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>

                                <h3 className="sidebar-title">Recent Posts</h3>
                                <div className="sidebar-item recent-posts">
                                    <RecentPosts />

                                    {/* <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-2.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <Link href="blog-single.html">
                                                Quidem autem et impedit
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div>

                                    <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-3.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <Link href="blog-single.html">
                                                Id quia et et ut maxime
                                                similique occaecati ut
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div>

                                    <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-4.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <Link href="blog-single.html">
                                                Laborum corporis quo dara net
                                                para
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div>

                                    <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-5.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <Link href="blog-single.html">
                                                Et dolores corrupti quae illo
                                                quod dolor
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div> */}
                                </div>

                                <h3 className="sidebar-title">Tags</h3>
                                <div className="sidebar-item tags">
                                    <ul>
                                        <li>
                                            <Link href="#">App</Link>
                                        </li>
                                        <li>
                                            <Link href="#">IT</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Business</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Mac</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Design</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Office</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Creative</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Studio</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Smart</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Tips</Link>
                                        </li>
                                        <li>
                                            <Link href="#">Marketing</Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* <div className="col-lg-4">
                            <div className="sidebar">
                                <h2 className="mb-4">Project information</h2>
                                <h3 className="sidebar-title">Client</h3>
                                <p>{headingName}</p>
                                <h3 className="sidebar-title mt-4">
                                    Scope of work
                                </h3>
                                <p>{state.category}</p>
                                <h3 className="sidebar-title mt-4">
                                    Project date
                                </h3>
                                <p>
                                    {moment(startDate).format('MMM-YYYY')} -
                                    {moment(endDate).format('MMM-YYYY')}
                                </p>

                                <h3 className="sidebar-title mt-4 ">
                                    Project Link
                                </h3>
                                <Link
                                    href={`https://${clientWebsite}`}
                                    target="_blank"
                                    // type="button"
                                    className="btn btn-light btn-sm d-flex align-items-center "
                                >
                                    <i className="bi bi-box-arrow-up-right me-1"></i>
                                    Check it out
                                </Link>
                              
                                <h3 className="sidebar-title mt-4 ">
                                    Technologies Used
                                </h3>
                                <div className="sidebar-item tags">
                                    <div className="d-flex flex-wrap">
                                        {state.arrTech &&
                                            state.arrTech.map(
                                                (e: any, i: number) => {
                                                    return (
                                                        <h4
                                                            style={{
                                                                cursor: 'pointer',
                                                            }}
                                                            key={i}
                                                        >
                                                            <span
                                                                style={{
                                                                    border: '1px solid #7666df',
                                                                    color: '#7666df',
                                                                }}
                                                                className="badge me-1 rounded-pill"
                                                            >
                                                                {e}
                                                            </span>
                                                        </h4>
                                                    )
                                                }
                                            )}

                                       
                                    </div>

                                    
                                </div>
                             
                            </div>
                           
                        </div> */}
                    </div>
                </div>
            </section>
        </main>
    )
}
export default BlogDetail
