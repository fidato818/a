import Ticket from '../blog-page'
const axios = require('axios')
let trendingTracks: any[] = []
export async function generateStaticParams() {
    try {
        const response = await axios.get(
            'https://adnan-ahmed.firebaseio.com/blogsData.json?print=pretty'
        )

        if (response.data) {
            trendingTracks = []
            for (var item of Object.values(response.data)) {
                trendingTracks.push(item)
            }
        }
    } catch (error) { 
        console.error(error)
    }

    return trendingTracks.map((post: any) => ({
        id: post.newPostKey,
    }))
}

export default function Page({ params }: { params: { id: string } }) {
    return <Ticket params={{ id: params.id }} />
}
