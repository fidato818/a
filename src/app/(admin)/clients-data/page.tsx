'use client'

import { useState } from 'react'
import dynamic from 'next/dynamic'

// Client Components:

const ComponentC = dynamic(() => import('./client-comp'), { ssr: false })

export default function ClientComponentExample() {
    return (
        <div>
            <ComponentC />
        </div>
    )
}
