"use client"
import React, { useEffect, useState } from 'react'
import styles from './style.module.css'

const Page = () => {

    const [isClient, setIsClient] = useState(false)

    useEffect(() => {
        setIsClient(true)
    }, [])
    return (
        <div className="container">
            <div className="row mt-4">
                <div className="col-xl-3 col-lg-3 ">
                    <div className={`${styles.card} ${styles.lBgCherry}`}>
                        <div className={`${styles.cardStatistic3} p-4`}>
                            <div
                                className={`${styles.cardIcon} ${styles.cardIconLarge} `}
                            >
                                <i className={`${styles.fas} bi bi-bing`}></i>
                            </div>
                            <div className="mb-4">
                                <h5 className="card-title mb-0 ">Blogs</h5>
                            </div>
                            <div className="row align-items-center mb-2 d-flex">
                                <div className="col-8">
                                    <h2 className="d-flex align-items-center mb-0">
                                        3,243
                                    </h2>
                                </div>
                                <div className="col-4 text-right">
                                    {/* <span>12.5% <i className="fa fa-arrow-up"></i></span> */}
                                </div>
                            </div>
                            <div
                                className=" mt-1 "
                                data-height="8"
                                style={{ height: '8px' }}
                            >
                                {/* <div className={`progress-bar ${styles.lBgCyan}"`} 
                                    role="progressbar" data-width="25%" aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} style={{ width: "25%" }}>

                                    </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-lg-3 ">
                    <div className={`${styles.card} ${styles.lBgBlueDark}`}>
                        <div className={`${styles.cardStatistic3} p-4`}>
                            <div
                                className={`${styles.cardIcon} ${styles.cardIconLarge} `}
                            >
                                <i
                                    className={`${styles.fas} bi bi-book-half`}
                                ></i>
                            </div>
                            <div className="mb-4">
                                <h5 className="card-title mb-0 ">Books</h5>
                            </div>
                            <div className="row align-items-center mb-2 d-flex">
                                <div className="col-8">
                                    <h2 className="d-flex align-items-center mb-0">
                                        15.07k
                                    </h2>
                                </div>
                                <div className="col-4 text-right">
                                    {/* <span>9.23% <i className="fa fa-arrow-up"></i></span> */}
                                </div>
                            </div>
                            {/* <div className="progress mt-1 " data-height="8" style={{ height: "8px" }}> */}
                            <div
                                className=" mt-1 "
                                data-height="8"
                                style={{ height: '8px' }}
                            >
                                {/* <div className={`progress-bar ${styles.lBgCyan}"`} role="progressbar" data-width="25%" aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} style={{ width: "25%" }}></div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-lg-3 ">
                    <div className={`${styles.card} ${styles.lBgGreenDark}`}>
                        <div className={`${styles.cardStatistic3} p-4`}>
                            <div
                                className={`${styles.cardIcon} ${styles.cardIconLarge} `}
                            >
                                <i
                                    className={`${styles.fas} bi bi-download`}
                                ></i>
                            </div>
                            <div className="mb-4">
                                <h5 className="card-title mb-0 ">
                                    Download Software
                                </h5>
                            </div>
                            <div className="row align-items-center mb-2 d-flex">
                                <div className="col-8">
                                    <h2 className="d-flex align-items-center mb-0">
                                        578
                                    </h2>
                                </div>
                                <div className="col-4 text-right">
                                    {/* <span>10% <i className="fa fa-arrow-up"></i></span> */}
                                </div>
                            </div>
                            <div
                                className=" mt-1 "
                                data-height="8"
                                style={{ height: '8px' }}
                            >
                                {/* <div className={`progress-bar ${styles.lBgOrange}"`} role="progressbar" data-width="25%" aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} style={{ width: "25%" }}></div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-lg-3 ">
                    <div className={`${styles.card} ${styles.lBgOrangeDark}`}>
                        <div className={`${styles.cardStatistic3} p-4`}>
                            <div
                                className={`${styles.cardIcon} ${styles.cardIconLarge} `}
                            >
                                <i className={`${styles.fas} bi bi-regex`}></i>
                            </div>
                            <div className="mb-4">
                                <h5 className="card-title mb-0 ">Projects</h5>
                            </div>
                            <div className="row align-items-center mb-2 d-flex">
                                <div className="col-8">
                                    <h2 className="d-flex align-items-center mb-0">
                                        $11.61k
                                    </h2>
                                </div>
                                <div className="col-4 text-right">
                                    {/* <span>2.5% <i className="fa fa-arrow-up"></i></span> */}
                                </div>
                            </div>
                            <div
                                className=" mt-1 "
                                data-height="8"
                                style={{ height: '8px' }}
                            >
                                {/* <div className={`progress-bar ${styles.lBgCyan}"`} role="progressbar" data-width="25%" aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} style={{ width: "25%" }}></div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Page
