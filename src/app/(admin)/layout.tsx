'use client'
import '../globals.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'aos/dist/aos.css'

import { usePathname } from 'next/navigation'
import AOS from 'aos'


import { useEffect } from 'react'
import NavAdmin from '@/components/NavAdmin'

// const inter = Outfit({ subsets: ['latin'] })

const AdminLayout = ({ children }: { children: React.ReactNode }) => {
    const pathname = usePathname()
    useEffect(() => {
        AOS.init({
            duration: 1000,
            easing: 'ease-in-out',
            once: true,
            mirror: false,
        })
    }, [])
    const splitPath = pathname.split('/')
    const hasUpperCase = (str: string) => /[A-Z]/.test(str)

    return (
        <>
            {splitPath[1] !== 'login' && <NavAdmin />}
            {children}
        </>
    )
}
export default AdminLayout
