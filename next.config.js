// /** @type {import('next').NextConfig} */
// const runtimeCaching = require("@ducanh2912/next-pwa/cache");
const isProduction = process.env.NODE_ENV === 'production'
const withPWA = require('@ducanh2912/next-pwa').default({
    dest: 'public',
    // register: true,
    // skipWaiting: true,
    disable: !isProduction,
    // runtimeCaching,
    // buildExcludes: [/middleware-manifest.json$/],
})
const nextConfig = withPWA({
    // experimental: {
    //     missingSuspenseWithCSRBailout: false,
    // },
    reactStrictMode: false,
    // output: 'export',
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'mega.nz',
            },
        ],
        unoptimized: true,
        domains: [
            'nextecommerce.web.app',
            'mega.nz',
            'drive.google.com',
            'firebasestorage.googleapis.com',
        ],
    },
})

module.exports = nextConfig
